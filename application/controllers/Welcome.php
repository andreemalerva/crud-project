<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $form_validation;
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form','url');
		$this->load->library('email');
	}

	public function index()
	{
		$this->load->view('firebase');
	}


	public function validar()
	{
		//echo "Datos cargados correctamente";
		$datos = $_POST;
		$this->validarDatos($datos);
		$this->enviarCorreo($datos);
		$datos = $this->limpiarDatos($datos);
		//$this->load->view('firebase');
		header('location:http://localhost/andreemalerva-proyectos/crud-project/');
		//$this->load->view('gracias');
		//header('location: https://andreemalerva.com/proyectos/crud-project/');
	}
	public function validarDatos($datos){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[2]|max_length[30]');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required|numeric|min_length[10]|max_length[12]');
        $this->form_validation->set_rules('correo', 'Correo', 'required|valid_email');
         
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('gracias');
        } else {
			return true;
			//phpinfo();
			/* $this->enviarCorreo($datos);
			$datos = $this->limpiarDatos($datos);
			$this->load->view('firebase'); */
			//redirect('/');
            //echo "Datos cargados correctamente";
        }
	}
	private function limpiarDatos($datos)
	{
		foreach ($datos as $key => $value) {
			$datos[$key] = trim($value);
			$datos[$key] = htmlspecialchars($value);
			$_SESSION[$key] = $value;
		}
		return $datos;
	}

	private function enviarCorreo($datos)
	{
		$datos = $_POST;
		ini_set('display_errors', 1);
		error_reporting(E_ALL);
		$from = "noreplay@andreemalerva.com";
		$to = "andreemalerva@gmail.com";
		$subject = "Nuevo contacto firebase";
		$message = "<ul>";
		$campos = array("nombre", "telefono", "correo");
		foreach ($campos as $value) {
			$message .= "<li>$value: $datos[$value]</li>";
		}
		$message .= "</ul>";
		//$message = "PHP mail works just fine";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers = "From:" . $from;
		mail($to, $subject, $message, $headers);
	}
}
