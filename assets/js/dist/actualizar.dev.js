"use strict";

var firebaseConfig = {
  apiKey: "AIzaSyBbgXuNFibNSfUhD-8_VG_kr1T4YvPfQl8",
  authDomain: "blancamalerva-proyectos.firebaseapp.com",
  projectId: "blancamalerva-proyectos",
  storageBucket: "blancamalerva-proyectos.appspot.com",
  messagingSenderId: "1039266809256",
  appId: "1:1039266809256:web:bcb5ce182c9c400c2f899d",
  measurementId: "G-JFBR45YJZE"
};
firebase.initializeApp(firebaseConfig);
firebase.analytics();
var database = firebase.database();
var referencia = database.ref("SitioBlancaCruz/");
var productos = {};
referencia.on('value', function (datos) {
  var a = datos.numChildren();
  document.getElementById("total").innerHTML = a;
  productos = datos.val();
  $.each(productos, function (indice, valor) {
    var prevDatos = "<tr>";
    prevDatos += '<td id="nombre2" >' + valor.nombre + "</td>";
    prevDatos += '<td id="correo2">' + valor.correo + "</td>";
    prevDatos += '<td id="telefono2">' + valor.telefono + "</td>";
    prevDatos += '<td id="' + indice + '">' + indice + "</td>";
    prevDatos += '<td class=""><button type="button" class="btn btn-warning mr-3" onclick="editarProducto(\'' + indice + '\')">Actualizar</button><button type="button" class="btn btn-danger" id="eliminar" data-toggle="modal" data-target="#exampleModal">Eliminar</button></td>';
    prevDatos += "</tr>";
    $(prevDatos).appendTo("#prueba");
  });
}, function (objetoError) {
  console.log('Error de lectura:' + objetoError.code);
});

function editarProducto(id) {
  window.name = id;
  $('#exampleModal').modal('show');
  resetBoton();
}

function resetBoton() {
  document.getElementById("nombre2").reset();
  document.getElementById("correo2").reset();
  document.getElementById("telefono2").reset();
}

var productoId = window.name;
console.log(productoId);
var nombre, correo, telefono;
var producto = {};
referencia.child(productoId).once('value', function (datos) {
  producto = datos.val();
  nombre = producto.nombre;
  correo = producto.correo;
  telefono = producto.telefono;
  $('#telefono').val(telefono);
  $('#correo').val(correo);
  $('#nombre').val(nombre);
});
$("#botonActualizar").click(function () {
  var telefono = $("#telefono").val();
  var correo = $("#correo").val();
  var nombre = $("#nombre").val();
  referencia.child(productoId).update({
    nombre: nombre,
    correo: correo,
    telefono: telefono
  }, alFinalizar);

  function alFinalizar(error) {
    if (error) {
      alert('Ha habido problemas al realizar la operación: ' + error.code);
    } else {
      alert('Operación realizada con éxito !');
      location.assign('firebase-update');
      console.log('se ha actualizado texto');
    }
  }
});