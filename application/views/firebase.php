<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Peticiones Firebase</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/css/firebase-actualizacion.css">
</head>

<body>
	<div class="container">
		<h1>Consulta por colección</h1>
		<h2>Tu coleccion es: <b id="coleccion">Sitio Web Datos/</b></h2>

		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nuevoUsuario">
			Nuevo usuario
		</button><br><br>
		<div class="numeroDatos">
		<label for="totales"> Total de datos:</label>
		<p id="total"></p>
		</div>
		<br>
		<div class="">
			<table class="table table-striped">
				<thead>
					<tr>
						<th scope="col">Nombre</th>
						<th scope="col">Email</th>
						<th scope="col">Teléfono</th>
						<th scope="col">Fecha Ingreso</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody id="datosBase">
				</tbody>
			</table>
		</div>


		<!-- Modal Actualizar Usuario-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Actualizar Usuario</h5>
						<button type="button" id="cierraBtn" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="error"></div>
						<div>
							<form action="" method="POST" id="formulario">
								<div class="row">
									<div class="col-2">
										<label for="nombre">Nombre:</label>
									</div>
									<div class="col-10" id="valor-nombre">
										<input type="text" class="w-100" name="nombre" id="nombre">
									</div>
									<div class="col-2 mt-2">
										<label for="nombre">Correo:</label>
									</div>
									<div class="col-10 mt-2">
										<input type="text" class="w-100" name="correo" id="correo">
									</div>
									<div class="col-2 mt-2">
										<label for="nombre">telefono:</label>
									</div>
									<div class="col-10 mt-2">
										<input type="tel" class="w-100" name="telefono" id="telefono">
									</div>
									<div id="fecha3" class="col-10 mt-2">

									</div>

									<div class="col-12 mt-4 d-flex justify-content-end">
										<button type="submit" class="btn btn-info" id="botonActualizar">ENVIAR</button>
									</div>
								</div>
							</form>
						</div>
						<div class="container-loader d-none" id="loader-page">
							<div class="loader"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- Modal Nuevo Usuario-->
		<div class="modal fade" id="nuevoUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Nuevo Usuario</h5>
						<button type="button" id="cierraBtnUsuario" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="error"></div>
						<div>
							<form action="" method="POST" id="formularioUsuario">
								<div class="row">
									<div class="col-2">
										<label for="nombre">Nombre:</label>
									</div>
									<div class="col-10" id="valor-nombre">
										<input type="text" class="w-100" name="nombre" id="nombre">
									</div>
									<div class="col-2 mt-2">
										<label for="nombre">Correo:</label>
									</div>
									<div class="col-10 mt-2">
										<input type="text" class="w-100" name="correo" id="correo">
									</div>
									<div class="col-2 mt-2">
										<label for="nombre">telefono:</label>
									</div>
									<div class="col-10 mt-2">
										<input type="tel"  class="w-100" name="telefono" id="telefono">
									</div>
									<div id="fecha3" class="col-10 mt-2"></div>

									<div class="col-12 mt-4 d-flex justify-content-end">
										<button class="btn btn-success" id="enviar">ENVIAR</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- FIREBASE -->
	<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-database.js"></script>
	<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-auth.js"></script>
	<script src="https://www.gstatic.com/firebasejs/8.3.0/firebase.js"></script>
	<script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-analytics.js"></script>
	<!-- jquery -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
	<script src="assets/js/actualizar.js"></script>
</body>

</html>
