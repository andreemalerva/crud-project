const firebaseConfig = {
	apiKey: "AIzaSyAljnRnxrCmf172486uukP4mZRpDBhIk1s",
	authDomain: "proyecto-crud-firebase-fefc6.firebaseapp.com",
	databaseURL: "https://proyecto-crud-firebase-fefc6-default-rtdb.firebaseio.com",
	projectId: "proyecto-crud-firebase-fefc6",
	storageBucket: "proyecto-crud-firebase-fefc6.appspot.com",
	messagingSenderId: "287444895885",
	appId: "1:287444895885:web:7431ef5f243dd426433ac4",
	measurementId: "G-6N69P4T55T"
};
firebase.initializeApp(firebaseConfig);
firebase.analytics();

var database = firebase.database();
var referencia = database.ref("web-datos/");
var productos = {};

referencia.on(
	"value",
	function (datos) {
		var a = datos.numChildren();
		document.getElementById("total").innerHTML = a;
		productos = datos.val();
		$.each(productos, function (indice, valor) {
			var prevDatos = "<tr>";
			prevDatos += '<th class="col-2" id="nombre2">' + valor.nombre + "</th>";
			prevDatos += '<td class="col-2" id="correo2">' + valor.correo + "</td>";
			prevDatos += '<td class="col-2" id="telefono2">' + valor.telefono + "</td>";
			prevDatos += '<td id="' + indice + '">' + indice + "</td>";
			/* prevDatos += '<td class="col-2" id="dateIngreso"><p>' + indice + "</p></td>"; */
			prevDatos +=
				'<td class="col-4"><div class="row d-flex justify-content-center m-1"><button type="button" class="btn btn-warning col mr-lg-1" onclick="editarProducto(\'' +
				indice +
				'\')">Actualizar</button><button type="button" class="col btn btn-danger col mt-md-1" id="eliminar" onclick="eliminar(\'' +
				indice +
				"')\">Eliminar</button></div></td>";
			prevDatos += "</tr>";
			$(prevDatos).appendTo("#datosBase");
		});
	},
	function (objetoError) {
		console.log("Error de lectura:" + objetoError.code);
	}
);

var validarFormulario = function (form) {
	form.validate({
		rules: {
			nombre: { required: true },
			telefono: {
				required: true,
				digits: true,
				minlength: 10,
				maxlength: 10,
			},
			correo: { required: true, email: true },
		},
		messages: {
			nombre: { required: "El campo es obligatorio" },
			correo: {
				required: "El campo es obligatorio",
				email: "Ingresa un email válido",
			},
			telefono: {
				required: "El campo es obligatorio",
				digits: "Ingresa números solamente",
				minlength: "Mínimo 0 dígitos",
				maxlength: "Máximo 10 dí­gitos",
			},
		},
	});
	return form.valid();
};

$.fn.serializeObject = function () {
	var o = {};
	this.find("[name]").each(function () {
		o[this.name] = this.value;
	});
	return o;
};

$('#enviar').click(function (e) {
	console.log('CLICK', e);
	var form = $("#formularioUsuario");
	if (validarFormulario(form)) {
		$("#enviar").attr("disabled", "true");
		$("#loader-page").removeClass("d-none");
		$("#loader-page").addClass("d-flex");
		let datosForm = form.serializeObject();
		let id = new Date();
		firebase
			.database()
			.ref("web-datos/" + id)
			.set(
				{
					nombre: datosForm.nombre,
					correo: datosForm.correo,
					telefono: datosForm.telefono,
				},
				(error) => {
					if (error) {
						console.log("error");
					} else {
						//form.submit();
						alert("Usuario Creado");
						location.reload();
					}
				}
			);
		console.log("enviados");
	} else {
		console.log("error");
	}
});

function editarProducto(id) {
	window.name = id;
	$("#exampleModal").modal("show");
	// $(".close").click(function(){
	// 	const form = document.getElementById('formulario');
	// 	form.reset();
	// 	console.log('estoy acà');
	// });
}

var productoId = window.name;
//console.log(productoId);
var nombre, correo, telefono;
var producto = {};

referencia.child(productoId).once("value", function (datos) {
	producto = datos.val();
	nombre = producto.nombre;
	correo = producto.correo;
	telefono = producto.telefono;
	$("#telefono").val(telefono);
	$("#correo").val(correo);
	$("#nombre").val(nombre);
});

$("#botonActualizar").click(function () {
	var telefono = $("#telefono").val();
	var correo = $("#correo").val();
	var nombre = $("#nombre").val();
	referencia.child(productoId).update(
		{
			nombre: nombre,
			correo: correo,
			telefono: telefono,
		},
		alFinalizar
	);
	function alFinalizar(error) {
		if (error) {
			alert("Ha habido problemas al realizar la operación: " + error.code);
		} else {
			alert("Operación realizada con éxito !");
			location.assign("firebase");
			console.log("se ha actualizado texto");
		}
	}
});

function eliminar(val) {
	if (confirm("¿Está seguro/a de que quiere borrar este artículo?") == true) {
		referencia.child(val).remove();
		$(document).ready(function () {
			alert("DATO ELIMINADO");
			location.reload();
		});
	}
}

$("form #telefono")
	.keypress(function (e) {
		if (isNaN(this.value + String.fromCharCode(e.charCode))) return false;
	})
	.on("cut copy paste", function (e) {
		e.preventDefault();
	});

/*----- Habilitar boton solamente cuando este validado el formulario--*/
$("form").on("change keyup", function () {
	var form = $(this);
	var btn = form.find("#enviar");

	if (validarFormulario(form)) {
		btn.attr("disabled", false);
		console.log("habilitado");
	} else {
		btn.attr("disabled", true);
		console.log("deshabilitado");
	}
});






function enviar(datos, form) {
	var url = "http://localhost/andreemalerva/proyecto/validar";
	$.ajax({
		url: url,
		method: "POST",
		data: datos,
		dataType: "JSON",
		success: function (r) {
			console.log(r);
			form.submit();
		},
		error: function (xhr) {
			console.log(xhr.status);
			console.log(xhr.responseText);
			form.submit();
		},
	});
}
